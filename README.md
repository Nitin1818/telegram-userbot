# Functions #
|Command  | What it does                                          | Syntax|
|---------|-------------------------------------------------------|--------|
|         | resends links, because some clients, don't show links | `[text](link.com)`|
|         | repeats a text several times in a message             | `text*amount`|
| #blink  | lets a text blink                                     | `#blink text that will blink`|
| #slide  | lets a text slid                                      | `#slide text that will slide`|
| #type   | lets a text look like written in realtime.            | `#type start text-end 1\|end 2\| end3...`|
| #trigger | replies automatic to an message with a specified text in it|add trigger: `#trigger trigger-reply` list trigger: `#listtrigger` delete trigger: `#deltrigger trigger`|
| #correction | edits automatic things in your message | add correction: `#correction from-to.` list corrections: `#correctionls` delete corrections: `#delcorrection from`|
| #set_welcome | sets a welcome message | `#setwelcome welcome message with %name for the name of the new group member`|
| #spam | spams an lorem impsum text 1000 that triggers bots. | `#spam` and to stop `#stop`|
| #often | sends one message 100 times | `#often message that will be send`|
| #ban | bans the person you reply to. | `#ban reason`|
| #kick | kicks the person you reply to. | `#kick reason`|
| #warn | warns the person you reply to. The punishment will be alsys greater | `#warn reason`|
| #restrict | restricts the person you reply to from sending messanges | `#restrict-reason-days-hours`
| #poll |create a poll| `#poll title \n option 1 \n option 2 \n option 3...`|
| #whois | show information about the user you reply to| `#whois`|
| #name | change the first name | `#name name`|
| #bio | change biographie | `#bio new bio`|
| #profilepic | set profile pic | `#profilepic` in reply to photo or in photo caption |
| #sleep | stop the bot for a specific time | `#sleep secs`|
| #mock | change text to mock style | `#mock text to mock`|
| #zal | change text to zalgo style | `#zal text to zalgo`|
| #bayr | change german text to bavarian| `#bayr text in bavarian`|
| #style | change text style so every message will be edited to one style of the above | `#style ( 0:off \| 1: random \| 2: zalgo \| 3: mock)` |
| #randint | sends a random integer between a and b | `#randint a b`|
| #run | run python code | `#run python code`|
| #members | lists all group members | `#members`|
| #admins | lists all admins | `#admins` |
| #member_stats | shows, how many admins, members, bots, deleted Accounts are in the group | `#member_stats`|
| #haste | puts text into a hastebin | `#haste` in reply or `#haste text`|
| #choice | chooses one item of a list | `#choice a,b,c,d` |
| #shuffle | shuffles a list | `#shuffle a,b,c,d` |
| random name | replaces command with random name | first name: `#firstname` last name: `#lastname` full name: `#fullname`|
| clipboard | creates an clipboard, where people can add stutt | `#cb Title`|
| #getchat | sends information about the group | `#getchat` |
| purge | deletes messages | `#purge` deletes all messages you send `#purger` deletes recent messages `purgeall` deletes all messages|
| self destruct | delete message after a specified time | `#sd secs message`|
| #loading | let a message look like it's loading, before it is shown | `#loading <message>`|
| #info | shows chat id and message id of message, that it is replied to | `#info` in reply|
