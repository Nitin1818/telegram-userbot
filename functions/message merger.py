from pyrogram import Client, Filters
from configparser import ConfigParser
config = ConfigParser()
config.read('config.ini')
pf = config['bot-config']['prefix']


@Client.on_message(Filters.text & Filters.me & ~Filters.edited & ~Filters.regex('^'+pf) & ~Filters.reply, group=10)
def merge(client, message):
    for x in client.iter_history(message.chat.id, 1, 1):
        pre = x
    if not pre:
        return
    if pre.from_user.id == 364133699:
        if pre.text:
            pre.edit(pre.text.markdown+'\n'+message.text.markdown)
        elif pre.caption:
            pre.edit(pre.caption.markdown + '\n' + message.text.markdown)
        else:
            return
        message.delete()

