from pyrogram import Client, Filters
from datetime import datetime
from configparser import ConfigParser
config = ConfigParser()
config.read('config.ini')
pf = config['bot-config']['prefix']


@Client.on_message(Filters.command('ping', pf) & Filters.me)
def ping(client, message):
    start = datetime.now()
    message.edit('War ich nicht')
    end = datetime.now()
    ms = (end-start).microseconds / 1000
    message.edit('{} ms'.format(ms))

