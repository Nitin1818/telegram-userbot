from pyrogram import Client, Filters


@Client.on_message(Filters.me & Filters.regex('^#as_sticker'))
def send_as_sticker(client, message):
    if message.photo:
        sticker = client.download_media(message.photo)
    else:
        sticker = client.download_media(message.document.file_id)
    client.send_sticker(message.chat.id, sticker)
    message.delete()


@Client.on_message(Filters.me & Filters.photo)
def convert_pic_to_sticker(client, message):
    print('converting to sticker')
    if not message.caption:
        sticker = client.download_media(message.photo)
        client.send_sticker(message.chat.id, sticker)
        message.delete()
