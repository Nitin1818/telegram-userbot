from pyrogram.errors.exceptions.bad_request_400 import MessageNotModified
from pyrogram import Client, Filters
from time import sleep
import random
from configparser import ConfigParser
from ascii import eggs, cakes
config = ConfigParser()
config.read('config.ini')
pf = config['bot-config']['prefix']

BLINK = '_'
TYPESTARTSPLIT = '-'
TYPETEXTSSPLIT = '|'
CURSOR = '**|**'
KNOB = '◉'
LINE = '─'
LOADINDESCRIPTION = 'Downloading message:'
LOADINGDONE = '`Succesfully downloaded message`'


def randfloat(start, end):
    return random.random() * (end - start) + start


@Client.on_message(Filters.text & Filters.outgoing & Filters.command('blink', pf))
def blink(client, message):

    text = message.text.markdown[7:]
    for x in range(100):
        message.edit(text)
        sleep(1.1)
        message.edit(BLINK * len(text))
        sleep(1.1)
    Client.edit_message_text(message.chat.id, message.message_id, text)


@Client.on_message(Filters.text & Filters.outgoing & Filters.command('slide', pf))
def slide(client, message):
    text = message.text[6:]
    for x in range(200):
        for n in range(len(text)):
            new_text = text[n:] + text[:n]
            if new_text != text:
                message.edit(text[n + 1:] + text[:n + 1])
                sleep(0.07)


@Client.on_message(Filters.command('type', pf) & Filters.me)
def type_write(client, message):
    command = ' '.join(message.command[1:])
    beginning_text = command.split(TYPESTARTSPLIT)
    texts = beginning_text[1].split(TYPETEXTSSPLIT)
    for char_n in range(len(beginning_text[0])):
        try:
            message.edit(beginning_text[0][:char_n] + CURSOR)
            sleep(randfloat(0.02, 0.05))
        except MessageNotModified:
            pass
    for text in texts:
        for x in range(len(text)):
            try:
                message.edit(beginning_text[0] + text[:x] + CURSOR)
                sleep(randfloat(0.02, 0.5))
            except MessageNotModified:
                pass
        for a in range(len(text), 0, -1):
            if text != texts[-1]:
                try:
                    message.edit(beginning_text[0] + text[:a] + CURSOR)
                    sleep(randfloat(0.02, 0.5))
                except MessageNotModified:
                    pass
    message.edit(beginning_text[0] + texts[-1])


eastercount = 0


# **Delete command before @Client.on_message... while easter time**
# TODO: fix the regex
# @Client.on_message(Filters.regex('(egg |easter |ei |bunny |oster |hase )'), 9)
def easteregg(client, message):
    global eastercount
    if message.reply_to_message:
        if message.reply_to_message.from_user.id == 364133699:
            if eastercount >= 2:
                reply = message.reply("It's easter soon")
                deterring = -1
                for x in range(50):
                    for egg in eggs:
                        reply.edit(egg)
                        sleep(0.5)
    eastercount += 1


@Client.on_message(Filters.command('loading', pf) & Filters.me)
def loading(client, message):
    knob = KNOB
    for x in range(15):
        message.edit(LOADINDESCRIPTION+'\n'
                     + (LINE*x)+knob+(LINE*(14-x))+str(round(100/14*x))+'%')
        sleep(0.5)
    sleep(1)
    message.edit(LOADINGDONE)
    sleep(1)
    message.edit(message.text.markdown.replace('#loading ', ''))


@Client.on_message(Filters.me & Filters.command('cake',  pf))
def happy_birthday(client, message):
    print('ok')
    for x in range(20):
        for cake in cakes:
            print(cake)
            message.edit(cake)
            sleep(1)

