from pyrogram import Client, Filters
from time import time
from bot import sleeptime


@Client.on_message(Filters.text & Filters.outgoing & Filters.regex(r'\w*\[.+\]\(.+\)\w*'), group=2)
def links(client, message):
    if sleeptime < time():
        client.edit_message_text(message.chat.id, message.message_id, message.text, parse_mode='markdown')
