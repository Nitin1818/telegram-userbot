from pyrogram import Client, Filters
import datetime
from configparser import ConfigParser
config = ConfigParser()
config.read('config.ini')
pf = config['bot-config']['prefix']
start = datetime.datetime.now()


@Client.on_message(Filters.me & Filters.command('repo', pf))
def repo(client, message):
    if len(message.command) > 1:
        message.edit('[You can see the file {} here](https://gitlab.com/ulPa/telegram-userbot/tree/master/'
                     'functions/{})'.format(message.command[1], message.command[1]))
        return
    message.edit('You can see the source code [here](https://gitlab.com/ulPa/telegram-userbot)')


@Client.on_message(Filters.me & Filters.command('uptime', pf))
def uptime(client, message):
    now = datetime.datetime.now()
    duration = now-start
    message.edit(str(duration).split('.')[0])


#@Client.on_message(group=8)
def print_text(client, message):
    print(message.text)


@Client.on_message(Filters.me & Filters.command('info', pf))
def get_info(client, message):
    if message.reply_to_message:
        message.edit('Chat id: {}, Message id: {}'.format(message.chat.id, message.reply_to_message.message_id))
    else:
        message.edit('Chat id : {}'.format(message.chat.id))
