from pyrogram import Client, Filters
from pyrogram.errors.exceptions.bad_request_400 import MessageTooLong


@Client.on_message(Filters.me & Filters.regex('^.+\*\d+$'), group=2)
def often(client, message):
    text = message.text
    text, n = text.split('*')
    sendtext = text * int(n)
    try:
        client.edit_message_text(message.chat.id, message.message_id, sendtext)
    except MessageTooLong:
        parts = []
        while len(sendtext) > 4096:
            parts.append(sendtext[:4096])
            sendtext = sendtext[4096:]
        client.edit_message_text(message.chat.id, message.message_id, parts[0])
        for part in parts[1:]:
            client.send_message(message.chat.id, part)
