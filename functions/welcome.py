from pyrogram import Client, Filters
from time import sleep
import pickle
from configparser import ConfigParser
config = ConfigParser()
config.read('config.ini')
pf = config['bot-config']['prefix']


@Client.on_message(Filters.new_chat_members | Filters.command('show_welcome', '#'))
def welcome(client, message):
    with open('welcome', 'rb') as f:
        welcomes = pickle.load(f)
    if message.chat.id in welcomes:
        if message.command:
            message.edit(welcomes[message.chat.id])
            return
        new_members = ", ".join(
            ["[{}](tg://user?id={})".format(i.first_name, i.id) for i in message.new_chat_members])

        text = welcomes[message.chat.id].replace('%name', new_members)

        # Send the welcome message
        client.send_message(message.chat.id,
                            text,
                            reply_to_message_id=message.message_id,
                            disable_web_page_preview=True)


@Client.on_message(Filters.me & Filters.command('setwelcome', '#'))
def setwelcome(client, message):
    with open('welcome', 'rb') as f:
        welcomes = pickle.load(f)
    welcomes[message.chat.id] = message.text.replace('#setwelcome', '')
    with open('welcome', 'wb') as f:
        pickle.dump(welcomes, f)
    message.edit('`Welcome Message succesfully set.`')
    sleep(3)
    message.delete()
