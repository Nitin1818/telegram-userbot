from pyrogram import Client, Filters
from time import sleep
from configparser import ConfigParser
config = ConfigParser()
config.read('config.ini')
pf = config['bot-config']['prefix']


@Client.on_message(Filters.me & Filters.command(['sd','selfdestruct'], pf))
def self_destruct(client, message):
    message.edit(' '.join(message.command[2:]))
    sleep(int(message.command[1]))
    message.delete()
