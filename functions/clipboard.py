from pyrogram import Client, Filters
from configparser import ConfigParser
config = ConfigParser()
config.read('config.ini')
pf = config['bot-config']['prefix']

# {chat id: {message id: [text lines]}}
MESSAGES = {}
ANSWERS = '**Antworten**'
REPLY = '{user}: {message}'
USER = '[{name}](tg://user?id={id})'
MSGLINK = '[{text}](t.me/c/{chat}/{message})'


def is_clipboardmessage(filter, message):
    if message.chat.id in MESSAGES:
        if message.reply_to_message.message_id in MESSAGES[message.chat.id]:
            return True
    return False


clipboardmessage = Filters.create('clipboardmessage', is_clipboardmessage)


@Client.on_message(Filters.command(['cb', 'clipboard'], pf) & Filters.me)
def create_clipboard(client, message):
    if '#cb' in message.text:
        msgtext = message.text.replace('#cb ', '')
    elif '#clipboard' in message.text:
        msgtext = message.text.replace('#clipboard', '')
    message.edit(msgtext)
    if message.chat.id in MESSAGES:
        MESSAGES[message.chat.id][message.message_id] = [msgtext, ANSWERS]
    else:
        MESSAGES[message.chat.id] = {message.message_id: [msgtext, ANSWERS]}


@Client.on_message(Filters.reply & clipboardmessage)
def reply_clipboard(client,  message):
    MESSAGES[message.chat.id][message.reply_to_message.message_id].append(
        REPLY.format(user=USER.format(name=message.from_user.first_name,
                                      id=message.from_user.id),
                     message=message.text.markdown if message.text else MSGLINK.format(chat=str(message.chat.id).replace('-100', ''),
                                                                                       message=message.message_id)))

    message.reply_to_message.edit('\n\n'.join(MESSAGES[message.chat.id][message.reply_to_message.message_id]))
    if message.text:
        message.delete()



