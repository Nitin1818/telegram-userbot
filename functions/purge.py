from pyrogram import Client, Filters
from configparser import ConfigParser
config = ConfigParser()
config.read('config.ini')
pf = config['bot-config']['prefix']


@Client.on_message(Filters.me & Filters.command('purger', pf))
def purge_me_r(client, message):
    messages = client.iter_history(message.chat.id)
    del_messages = []
    msgs = 0
    for msg in messages:
        msgs += 1
        if msg.from_user.id == 364133699:
            del_messages.append(msg.message_id)
    client.delete_messages(message.chat.id, del_messages)


@Client.on_message(Filters.me & Filters.command('purge', pf))
def purge_me(client, message):
    messages = client.iter_history(message.chat.id)
    for msg in messages:
        if msg.from_user.id == 364133699:
            msg.delete()


@Client.on_message(Filters.me & Filters.command('purgeall', pf))
def purge_all(client, message):
    messages = client.iter_history(message.chat.id)
    del_messages = []
    for msg in messages:
        del_messages.append(msg.message_id)
        msg.delete()
