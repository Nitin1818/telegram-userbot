from pyrogram import Client, Filters
from bot import sleeptime
from time import time, sleep
from configparser import ConfigParser
config = ConfigParser()
config.read('config.ini')
pf = config['bot-config']['prefix']


# this script is written by colin shark <colinshark.de> and customized by ulPa <gitlab.com/ulPa>
@Client.on_message(Filters.command("poll", pf) & Filters.me)
def make_a_poll(bot, message):
    if sleeptime < time():
        cmd = message.command
        if len(cmd) == 1:
            message.edit("`Not enough arguments`")
        elif len(cmd) > 1:
            poll = message.text[6:].split('\n')
            if len(poll[1:]) < 2:
                message.edit("`Not enough answers`")
            elif len(poll[1:]) > 10:
                message.edit("`Too many answers`")
            else:

                bot.send_poll(
                    chat_id=message.chat.id,
                    question=poll[0],
                    options=poll[1:]
                )
    sleep(3)
    message.delete()
