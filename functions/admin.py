from pyrogram import Client, Filters
from time import mktime
import datetime
import pickle
from constants import util
from interval import IntervalHelper
import configparser
config = configparser.ConfigParser()
config.read('config.ini')
pf = config['bot-config']['prefix']

BANNED = '{name} is #banned for {reason}!'
KICKED = '{name} is #kicked for {reason}!'
RESTRICTED = '{name} is #restricted for {reason}!'
WARNED = '{name} is #warned for {reason} `{count}`!'
WARN_ACTION_REASON = 'having {count} warns'


@Client.on_message(Filters.text & Filters.outgoing & Filters.reply & Filters.command('ban', pf) & Filters.group)
def ban_reply(client, message):
    ban(client, message.chat, message.reply_to_message.from_user, ' '.join(message.command[1:]))


def ban(client, chat, user, reason, message_to_edit=None):
    client.kick_chat_member(chat.id, user.id)
    msg = BANNED.format(name=util.USER.format(first_name=user.first_name,
                                                    id=user.id),
                              reason=reason)
    if message_to_edit:
        message_to_edit.edit(msg)
    else:
        client.send_message(chat_id=chat.id, text=msg)


@Client.on_message(Filters.me & Filters.command('kick', pf))
def kick_reply(client, message):
    kick(client, message.reply_to_message.from_user, message.chat, ' '.join(message.command[1:]),
         message_to_edit=message)


def kick(client, user, chat, reason, message_to_edit=None):
    client.kick_chat_member(chat.id, user.id)
    client.unban_chat_member(chat.id, user.id)
    if message_to_edit:
        message_to_edit.edit(KICKED.format(name=util.USER.format_map(first_name=user.first_name,
                                                                     id=user.id),
                                           reson=reason))
    else:
        client.send_message(chat_id=chat.id,
                            text=KICKED.format(name=util.USER.format_map(first_name=user.first_name,
                                                                         id=user.id),
                                               reason=reason))


@Client.on_message(Filters.me & Filters.command('warn', pf))
def warn_user(client, message):
    warn(client, message.chat, message.reply_to_message.from_user, ' '.join(message.command[1:]), message)


def warn(client, chat, user, reason, message_to_edit=None):
    # increasing warn count
    with open('warn', 'rb') as f:
        warnings = pickle.load(f)
    if chat.id in warnings:
        if user.id in warnings[chat.id]:
            warnings[chat.id][user.id] += 1
        else:
            warnings[chat.id][user.id] = 1
    else:
        warnings[chat.id] = {user.id: 1}
    with open('warn', 'wb') as f:
        pickle.dump(warnings, f)
    # punish person
    warns = warnings[chat.id][user.id]
    if warns == 1:
        restrict(client, chat, user, '1h', WARN_ACTION_REASON.format(count=warns))
    elif warns == 2:
        restrict(client, chat, user, '1w', WARN_ACTION_REASON.format(count=warns))
    elif warns == 3:
        kick(client, user, chat, WARN_ACTION_REASON.format(count=warns))
    elif warns == 4:
        ban(client, user, chat, WARN_ACTION_REASON.format(count=warns))
    elif warns == 5:
        pass

    if message_to_edit:
        message_to_edit.edit(WARNED.format(name=util.USER.format(first_name=user.first_name,
                                                                 id=user.id),
                                           reason=reason,
                                           count=warns))
    else:
        client.send_message(chat_id=chat.id,
                            text=WARNED.format(name=util.USER.format(first_name=user.first_name,
                                                                     id=user.id),
                                               reason=reason,
                                               count=warns))


@Client.on_message(Filters.me & Filters.command('restrict', pf))
def restrict_reply(client, message):
    time, reason = message.text.replace('#restrict ', '').split('-')
    interval = IntervalHelper(time)
    restrict(client, message.chat, message.reply_to_message.from_user, interval.to_timedelta(), reason, message_to_edit=message)


def restrict(client, chat, user, time, reason, message_to_edit=None):
    now = datetime.datetime.now()
    time = IntervalHelper(time).to_timedelta()
    until = now+time
    client.restrict_chat_member(chat.id,
                                user.id,
                                until_date=int(mktime(until.timetuple()) + 1e-6 * until.microsecond),
                                can_send_messages=False,
                                can_change_info=False,
                                can_invite_users=False,
                                can_pin_messages=False)
    message_to_edit.edit(RESTRICTED.format(name=util.USER.format(first_name=user.first_name,
                                                                       id=user.id),
                                                 reason=reason))


# TODO that just forwards the message to spamwat.ch
@Client.on_message(Filters.me & Filters.command('rspam', pf) & Filters.reply)
def report_spam(client, message):
    message.edit('ok')
    message.reply_to_message.forward(643560883)
