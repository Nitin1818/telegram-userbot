from pyrogram import Client, Filters
import random
import names
from configparser import ConfigParser
config = ConfigParser()
config.read('config.ini')
pf = config['bot-config']['prefix']


@Client.on_message(Filters.command('randint', pf))
def send_randint(client, message):
    try:
        a = int(message.command[1])
        b = int(message.command[2])
    except ValueError:
        message.edit('`Das sind keine Integer`')
        return
    message.edit(str(random.randint(a, b)))


@Client.on_message(Filters.command('choice', pf))
def send_choice(client, message):
    items = message.text.replace('#choice ', '').split(',')
    message.edit(random.choice(items))


@Client.on_message(Filters.command('shuffle',pf))
def send_shuffle(client, message):
    items = message.text.replace('#shuffle ', '').split(',')
    random.shuffle(items)
    message.edit(','.join(items))


@Client.on_message(Filters.regex('#firstname') & Filters.me, group=4)
def first_name(client, message):
    message.edit(message.text.replace('#firstname', names.get_first_name()))


@Client.on_message(Filters.regex('#lastname') & Filters.me, group=5)
def last_name(client, message):
    message.edit(message.text.replace('#lastname', names.get_last_name()))


@Client.on_message(Filters.regex('#fullname') & Filters.me, group=4)
def full_name(client, message):
    message.edit(message.text.replace('#fullname', names.get_full_name()))




